$(document).ready(function () {
  function getCottonProducts() {
    return [
      {
        id: 1,
        name: 'CT01',
        description: 'Cotton 100%',
        img: 'https://pbs.twimg.com/profile_images/378800000532546226/dbe5f0727b69487016ffd67a6689e75a_400x400.jpeg',
        selected: false,
      },
      {
        id: 2,
        name: 'CT01',
        description: 'Cotton 100%',
        img: 'https://pbs.twimg.com/profile_images/378800000532546226/dbe5f0727b69487016ffd67a6689e75a_400x400.jpeg',
        selected: false,
      },
      {
        id: 3,
        name: 'CT01',
        description: 'Cotton 100%',
        img: 'https://pbs.twimg.com/profile_images/378800000532546226/dbe5f0727b69487016ffd67a6689e75a_400x400.jpeg',
        selected: false,
      },
      {
        id: 4,
        name: 'CT01',
        description: 'Cotton 100%',
        img: 'https://pbs.twimg.com/profile_images/378800000532546226/dbe5f0727b69487016ffd67a6689e75a_400x400.jpeg',
        selected: false,
      },
      {
        id: 5,
        name: 'CT01',
        description: 'Cotton 100%',
        img: 'https://pbs.twimg.com/profile_images/378800000532546226/dbe5f0727b69487016ffd67a6689e75a_400x400.jpeg',
        selected: false,
      },
      {
        id: 6,
        name: 'CT01',
        description: 'Cotton 100%',
        img: 'https://pbs.twimg.com/profile_images/378800000532546226/dbe5f0727b69487016ffd67a6689e75a_400x400.jpeg',
        selected: false,
      },
      {
        id: 7,
        name: 'CT01',
        description: 'Cotton 100%',
        img: 'https://pbs.twimg.com/profile_images/378800000532546226/dbe5f0727b69487016ffd67a6689e75a_400x400.jpeg',
        selected: false,
      },
      {
        id: 8,
        name: 'CT01',
        description: 'Cotton 100%',
        img: 'https://pbs.twimg.com/profile_images/378800000532546226/dbe5f0727b69487016ffd67a6689e75a_400x400.jpeg',
        selected: false,
      },
      {
        id: 9,
        name: 'CT01',
        description: 'Cotton 100%',
        img: 'https://pbs.twimg.com/profile_images/378800000532546226/dbe5f0727b69487016ffd67a6689e75a_400x400.jpeg',
        selected: false,
      },
      {
        id: 10,
        name: 'CT01',
        description: 'Cotton 100%',
        img: 'https://pbs.twimg.com/profile_images/378800000532546226/dbe5f0727b69487016ffd67a6689e75a_400x400.jpeg',
        selected: false,
      },
      {
        id: 11,
        name: 'CT01',
        description: 'Cotton 100%',
        img: 'https://pbs.twimg.com/profile_images/378800000532546226/dbe5f0727b69487016ffd67a6689e75a_400x400.jpeg',
        selected: false,
      },
      {
        id: 12,
        name: 'CT01',
        description: 'Cotton 100%',
        img: 'https://pbs.twimg.com/profile_images/378800000532546226/dbe5f0727b69487016ffd67a6689e75a_400x400.jpeg',
        selected: false,
      }
    ];
  }
  function getBackground3dProducts() {
    return [
      {
        id: 1,
        name: 'Biển đảo Việt Nam qua hàng ngàn thế hệ qua to vl nhé mấy bạn',
        description: '1234567',
        img: 'https://pbs.twimg.com/profile_images/378800000532546226/dbe5f0727b69487016ffd67a6689e75a_400x400.jpeg',
        selected: false,
      },
      {
        id: 2,
        name: 'Biển đảo Việt Nam qua hàng ngàn thế hệ qua to vl nhé mấy bạn',
        description: '1234567',
        img: 'https://pbs.twimg.com/profile_images/378800000532546226/dbe5f0727b69487016ffd67a6689e75a_400x400.jpeg',
        selected: false,
      },
      {
        id: 3,
        name: 'Biển đảo Việt Nam qua hàng ngàn thế hệ qua to vl nhé mấy bạn',
        description: '1234567',
        img: 'https://pbs.twimg.com/profile_images/378800000532546226/dbe5f0727b69487016ffd67a6689e75a_400x400.jpeg',
        selected: false,
      }
    ];
  }

  const maxAllowSelectProduct = 6;
  const cottonProducts = getCottonProducts();
  const background3dProducts = getBackground3dProducts();

  renderCottonProducts(cottonProducts);
  renderBackground3dProducts(background3dProducts);

  function renderCottonProducts(products) {
    const productElements = products.map(product => `
      <div class="col-2 product-item" data-id="${product.id}">
        <label class="labelradio">
          <img class="card-img-top"
            src="${product.img}"
            alt="Card image cap" />
          <div class="py-3 px-2">
            <h3 class="title">${product.name}</h3>
            <div class="d-flex align-items-center justify-content-between mt-1">
              <p class="description">
                ${product.description}
              </p>
              <a class="edit-button">
                Sửa
              </a>
            </div>
          </div>
          <input type="checkbox" ${product.selected ? `checked="checked"` : ''}>
          <span class="checkboxcus"></span>
          <div class="bg"></div>
        </label>
      </div>
    `);

    $('.cotton-product-list').append(productElements);
  }
  function renderBackground3dProducts(products) {
    const productElements = products.map(product => `
      <div class="col-4 product-item" data-id="${product.id}">
        <label class="labelradio">
          <img class="card-img-top"
            src="${product.img}"
            alt="Card image cap" />
          <div class="py-3 px-2">
            <h3 class="title">${product.name}</h3>
            <div class="d-flex align-items-center justify-content-between mt-1">
              <p class="description">
                ${product.description}
              </p>
            </div>
          </div>
          <input type="checkbox" ${product.selected ? `checked="checked"` : ''}>
          <span class="checkboxcus"></span>
          <div class="bg"></div>
        </label>
      </div>
    `);

    $('.background-3d-product-list').append(productElements);
  }
  const selectedCottonProducts = cottonProducts.filter(product => product.selected);
  const selectedBackground3dProducts = background3dProducts.filter(product => product.selected);
  const totalSelectedProducts = selectedCottonProducts.length + selectedBackground3dProducts.length;
  $('.select-count').text(totalSelectedProducts);

  $('.load-more-button').click(function () {
    const pageTabsElement = $('.page-tabs');
    const activePageTab = pageTabsElement.find('.nav-item .nav-link.active');
    activePageTab.closest('.nav-item')
    const activePageTabIndex = pageTabsElement.find('.nav-item').index(activePageTab.closest('.nav-item'));
    let products = [];

    if (activePageTabIndex === 0) {
      products = getCottonProducts();
      cottonProducts.push(products);
      renderCottonProducts(products);
    } else {
      products = getBackground3dProducts();
      background3dProducts.push(products);
      renderBackground3dProducts(products);
    }
  });

  $(document).on('click', '.cotton-product-list.product-list .product-item', function (e) {
    e.preventDefault();

    const id = $(this).data('id');
    const inputElement = $(this).find('input');
    const checked = inputElement[0].checked;
    const index = cottonProducts.findIndex(product => product.id === id);
    const selectedCottonProducts = cottonProducts.filter(product => product.selected);
    const selectedBackground3dProducts = background3dProducts.filter(product => product.selected);
    const totalSelectedProducts = selectedCottonProducts.length + selectedBackground3dProducts.length;

    if (checked) {
      cottonProducts[index].selected = false;
      inputElement.prop('checked', false);
      $('.select-count').text(totalSelectedProducts - 1);
    } else {
      if (totalSelectedProducts < maxAllowSelectProduct) {
        cottonProducts[index].selected = true;
        inputElement.prop('checked', true);
        $('.select-count').text(totalSelectedProducts + 1);
      }
    }
  });
  $(document).on('click', '.background-3d-product-list.product-list .product-item', function (e) {
    e.preventDefault();

    const id = $(this).data('id');
    const inputElement = $(this).find('input');
    const checked = inputElement[0].checked;
    const index = background3dProducts.findIndex(product => product.id === id);
    const selectedCottonProducts = cottonProducts.filter(product => product.selected);
    const selectedBackground3dProducts = background3dProducts.filter(product => product.selected);
    const totalSelectedProducts = selectedCottonProducts.length + selectedBackground3dProducts.length;

    if (checked) {
      background3dProducts[index].selected = false;
      inputElement.prop('checked', false);
      $('.select-count').text(totalSelectedProducts - 1);
    } else {
      if (totalSelectedProducts < maxAllowSelectProduct) {
        background3dProducts[index].selected = true;
        inputElement.prop('checked', true);
        $('.select-count').text(totalSelectedProducts + 1);
      }
    }
  });
});
