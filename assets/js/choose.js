$(document).ready(function () {
  function initStep1Event() {
    const initialState = {
      products: [
        {
          id: 1,
          name: 'Áo nam phổ thông',
          img: './../assets/images/product-image.jpg',
          selected: false,
        },
        {
          id: 2,
          name: 'Áo nam phổ thông',
          img: './../assets/images/product-image.jpg',
          selected: true
        },
        {
          id: 3,
          name: 'Áo nam phổ thông',
          img: './../assets/images/product-image.jpg',
          selected: false
        },
        {
          id: 4,
          name: 'Áo nam phổ thông',
          img: './../assets/images/product-image.jpg',
          selected: false
        },
        {
          id: 5,
          name: 'Áo nam phổ thông',
          img: './../assets/images/product-image.jpg',
          selected: true
        },
        {
          id: 6,
          name: 'Áo nam phổ thông',
          img: './../assets/images/product-image.jpg',
          selected: false
        },
        {
          id: 7,
          name: 'Áo nam phổ thông',
          img: './../assets/images/product-image.jpg',
          selected: false
        }
      ],
      maxAllowSelectProduct: 2,
    };
    const SELECT_PRODUCT_ACTION = 'SELECT_PRODUCT_ACTION';
    const UNSELECT_PRODUCT_ACTION = 'UNSELECT_PRODUCT_ACTION';

    const selectProduct = (id) => ({
      type: SELECT_PRODUCT_ACTION,
      payload: id
    });
    const unselectProduct = (id) => ({
      type: UNSELECT_PRODUCT_ACTION,
      payload: id
    });

    const reducer = (state, action) => {
      switch (action.type) {
        case SELECT_PRODUCT_ACTION: {
          const index = state.products.findIndex(product => product.id === action.payload);
          const selectedProducts = state.products.filter(product => product.selected);
          let newProducts = [ ...state.products ];

          if (selectedProducts.length < state.maxAllowSelectProduct) {
            newProducts = [
              ...state.products.slice(0, index),
              {...state.products[index], selected: true },
              ...state.products.slice(index + 1),
            ];
          }

          return { ...state, products: newProducts };
        }
        case UNSELECT_PRODUCT_ACTION: {
          const index = state.products.findIndex(product => product.id === action.payload);
          const newProducts = [
            ...state.products.slice(0, index),
            {...state.products[index], selected: false},
            ...state.products.slice(index + 1),
          ];

          return { ...state, products: newProducts };
        }
        default: {
          return { ...state };
        };
      }
    };

    const store = createStore(initialState, reducer);
    store.subscribe(render);

    render();

    function render() {
      const state = store.getState();
      const productsElement = state.products.map(product => {
        let buttonHtml = `<button class="btn btn-outline-primary mb-3 select-button" data-id="${product.id}">Chọn</button>`;
  
        if (product.selected) {
          buttonHtml = `<button class="btn btn-primary mb-3 unselect-button" data-id="${product.id}">BỎ CHỌN</button>`;
        }
  
        return `
          <div class="col-md-3 product-item">
            <div class="card-product">
              <div>
                <img class="card-img-top"
                  src="${product.img}"
                  alt="Card image cap" />
              </div>
              <div class="card-body">
                <h5 class="card-title title">${product.name}</h5>
                <div class="row">
                  ${buttonHtml}
                </div>
              </div>
            </div>
          </div>
        `;
      });
      const selectedProductIndexs = state.products.map((product, index) => product.selected ? index : undefined).filter(index => index !== undefined);
      const selectedProductsElement = selectedProductIndexs.map(index => productsElement[index]);
  
      $('#step-1 .select-count').text(`${selectedProductsElement.length}/${state.maxAllowSelectProduct}`);
      $('#step-1 .selected-product-list').html(selectedProductsElement);
      $('#step-1 .product-list:not(.selected-product-list)').html(productsElement);
    }

    $(document).on('click', '#step-1 .select-button', function () {
      const id = $(this).data('id');
      store.dispatch(selectProduct(id));
    });
    $(document).on('click', '#step-1 .unselect-button', function () {
      const id = $(this).data('id');
      store.dispatch(unselectProduct(id));
    });
  }

  function initStep2Event() {
    const initialState = {
      specialProducts: [
        {
          id: 1,
          name: 'Áo thun ngắn tay',
          img: './../assets/images/timthumb.jpg',
          selected: true,
        }
      ],
      products: [
        {
          id: 1,
          name: 'T-shirt tay ngắn cổ tròn',
          description: 'Polyester 100%',
          img: './../assets/images/product-image.jpg',
          selected: false,
        },
        {
          id: 2,
          name: 'T-shirt tay ngắn cổ tròn',
          description: 'Polyester 100%',
          img: './../assets/images/product-image.jpg',
          selected: false,
        },
        {
          id: 3,
          name: 'T-shirt tay ngắn cổ tròn',
          description: 'Polyester 100%',
          img: './../assets/images/product-image.jpg',
          selected: false,
        },
        {
          id: 4,
          name: 'T-shirt tay ngắn cổ tròn',
          description: 'Polyester 100%',
          img: './../assets/images/product-image.jpg',
          selected: false,
        },
        {
          id: 5,
          name: 'T-shirt tay ngắn cổ tròn',
          description: 'Polyester 100%',
          img: './../assets/images/product-image.jpg',
          selected: false,
        },
        {
          id: 6,
          name: 'T-shirt tay ngắn cổ tròn',
          description: 'Polyester 100%',
          img: './../assets/images/product-image.jpg',
          selected: false,
        },
        {
          id: 7,
          name: 'T-shirt tay ngắn cổ tròn',
          description: 'Polyester 100%',
          img: './../assets/images/product-image.jpg',
          selected: false,
        }
      ],
      maxAllowSelectProduct: 3
    };
    const SELECT_SPECIAL_PRODUCT_ACTION = 'SELECT_SPECIAL_PRODUCT_ACTION';
    const UNSELECT_SPECIAL_PRODUCT_ACTION = 'UNSELECT_SPECIAL_PRODUCT_ACTION';
    const SELECT_PRODUCT_ACTION = 'SELECT_PRODUCT_ACTION';
    const UNSELECT_PRODUCT_ACTION = 'UNSELECT_PRODUCT_ACTION';

    const selectSpecialProduct = (id) => ({
      type: SELECT_SPECIAL_PRODUCT_ACTION,
      payload: id,
    });
    const unselectSpecialProduct = (id) => ({
      type: UNSELECT_SPECIAL_PRODUCT_ACTION,
      payload: id,
    });
    const selectProduct = (id) => ({
      type: SELECT_PRODUCT_ACTION,
      payload: id,
    });
    const unselectProduct = (id) => ({
      type: UNSELECT_PRODUCT_ACTION,
      payload: id,
    });

    const reducer = (state, action) => {
      switch (action.type) {
        case SELECT_SPECIAL_PRODUCT_ACTION: {
          const index = state.specialProducts.findIndex(product => product.id === action.payload);
          const selectedSpecialProducts = state.specialProducts.filter(product => product.selected);
          const selectedProducts = state.products.filter(product => product.selected);
          let newSpecialProducts = [ ...state.specialProducts ];

          if ((selectedSpecialProducts.length + selectedProducts.length) < state.maxAllowSelectProduct) {
            newSpecialProducts = [
              ...state.specialProducts.slice(0, index),
              {...state.specialProducts[index], selected: true },
              ...state.specialProducts.slice(index + 1),
            ];
          }

          return { ...state, specialProducts: newSpecialProducts };
        }
        case UNSELECT_SPECIAL_PRODUCT_ACTION: {
          const index = state.specialProducts.findIndex(product => product.id === action.payload);
          const newSpecialProducts = [
            ...state.specialProducts.slice(0, index),
            {...state.specialProducts[index], selected: false},
            ...state.specialProducts.slice(index + 1),
          ];

          return { ...state, specialProducts: newSpecialProducts };
        }
        case SELECT_PRODUCT_ACTION: {
          const index = state.products.findIndex(product => product.id === action.payload);
          const selectedProducts = state.products.filter(product => product.selected);
          const selectedSpecialProducts = state.specialProducts.filter(product => product.selected);
          let newProducts = [ ...state.products ];

          if ((selectedSpecialProducts.length + selectedProducts.length) < state.maxAllowSelectProduct) {
            newProducts = [
              ...state.products.slice(0, index),
              {...state.products[index], selected: true },
              ...state.products.slice(index + 1),
            ];
          }

          return { ...state, products: newProducts };
        }
        case UNSELECT_PRODUCT_ACTION: {
          const index = state.products.findIndex(product => product.id === action.payload);
          const newProducts = [
            ...state.products.slice(0, index),
            {...state.products[index], selected: false},
            ...state.products.slice(index + 1),
          ];

          return { ...state, products: newProducts };
        }
        default: {
          return { ...state };
        };
      }
    };

    const store = createStore(initialState, reducer);

    store.subscribe(render);
    render();

    function render() {
      const state = store.getState();

      const specialProductElements = state.specialProducts.map(product => {
        let buttonText = 'CHỌN';
        let buttonClass = 'select-product-button';

        if (product.selected) {
          buttonText = 'BỎ CHỌN';
          buttonClass = 'unselect-product-button';
        }

        return `
          <div class="col-md-4 product-item">
            <label class="card labelradio p-3">
              <div class="d-flex justify-content-between align-items-center">
                <div>
                  <p>Kiểu cơ bản:<br />${product.name}</p>
                </div>
                <img src="${product.img}" />
              </div>
              <p style="padding-top: 10px" class="mb-0">Đây là mô tả cho kiểu áo này nhé. Lorem Ipsum chỉ đơn giản là một đoạn
                văn bản giả, được dùng vào việc trình bày và dàn trang phục vụ cho in ấn. Lorem Ipsum đã được sử
                dụng như một</p>
              <input type="checkbox"${product.selected ? ` checked="checked"` : ''} data-id=${product.id}>
              <span class="checkboxcus"></span>
              <div class="bg"></div>
            </label>
            <div class="product-action">
              <button class="btn-border ${buttonClass}" data-id="${product.id}">
                ${buttonText}
              </button>
              <button class="card-text chinhsua btn btn-outline refine-button">CHỈNH SỬA</button>
            </div>
          </div>
        `;
      });
      const productElements = state.products.map(product => {
        let buttonText = 'CHỌN';
        let buttonClass = 'select-product-button';

        if (product.selected) {
          buttonText = 'BỎ CHỌN';
          buttonClass = 'unselect-product-button';
        }

        return `
          <div class="col-md-4 product-item">
            <label class="card flex-row labelradio d-flex p-3">
              <img class="product-image" src="${product.img}">
              <div class="ml-3">
                <p class="tieude" style="font-size: 16px">${product.name}</p>
                <p class="tieudexam description" style="font-size: 12px">${product.description}</p>
                <div class="d-flex mt-2">
                  <div style="width: 48px; height: 48px; background: red; border-radius: 4px"></div>
                  <div class="ml-2">
                    <p class="normaltext name">Quả dứa</p>
                    <span class="normaltext description" style="color: #6C738D;">PC21</span>
                  </div>
                </div>
              </div>
              <input type="checkbox"${product.selected ? ` checked="checked"` : ''} data-id=${product.id}>
              <span class="checkboxcus"></span>
              <div class="bg"></div>
            </label>
            <div class="product-action">
              <button class="btn-border ${buttonClass}" data-id="${product.id}">
                ${buttonText}
              </button>
              <button class="card-text chinhsua btn btn-outline refine-button">CHỈNH SỬA</button>
            </div>
          </div>
        `;
      });
      const selectedProducts = state.products.filter(product => product.selected);
      const selectedSpecialProducts = state.specialProducts.filter(product => product.selected);

      $('#step-2 .select-count').text(`${selectedProducts.length + selectedSpecialProducts.length}/${state.maxAllowSelectProduct}`);
      $('#step-2 .special-product-list').html(specialProductElements);
      $('#step-2 .basic-product-list').html(productElements);
    }

    $(document).on('change', '#step-2 .special-product-list input[type="checkbox"]', function () {
      const checked = this.checked;
      const id = $(this).data('id');

      if (checked) {
        store.dispatch(selectSpecialProduct(id));
      } else {
        store.dispatch(unselectSpecialProduct(id));
      }
    });
    $(document).on('click', '#step-2 .special-product-list .select-product-button', function () {
      const id = $(this).data('id');
      store.dispatch(selectSpecialProduct(id));
    });
    $(document).on('click', '#step-2 .special-product-list .unselect-product-button', function () {
      const id = $(this).data('id');
      store.dispatch(unselectSpecialProduct(id));
    });

    $(document).on('change', '#step-2 .basic-product-list input[type="checkbox"]', function () {
      const checked = this.checked;
      const id = $(this).data('id');

      if (checked) {
        store.dispatch(selectProduct(id));
      } else {
        store.dispatch(unselectProduct(id));
      }
    });
    $(document).on('click', '#step-2 .basic-product-list .select-product-button', function () {
      const id = $(this).data('id');
      store.dispatch(selectProduct(id));
    });
    $(document).on('click', '#step-2 .basic-product-list .unselect-product-button', function () {
      const id = $(this).data('id');
      store.dispatch(unselectProduct(id));
    });
    $(document).on('click', '#step-2 .basic-product-list .refine-button', function () {
      $('#edit-position-modal-2').modal('show');
    })
  }

  initStep1Event();
  initStep2Event();
});
