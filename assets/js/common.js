$(document).ready(function () {
  const createStore = (initialState, reducer) => {
    let state = initialState;
    const listeners = [];

    const getState = () => state;
    const dispatch = (action) => {
      state = reducer(state, action);
      listeners.forEach(listener => listener());
    }
    const subscribe = listener => {
      if (!listeners.includes(listener)) {
        listeners.push(listener);
      }
    }

    return {
      getState,
      dispatch,
      subscribe
    };
  };

  window.createStore = createStore;
});
